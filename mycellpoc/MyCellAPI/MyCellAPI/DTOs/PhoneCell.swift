//
//  PhoneCell.swift
//  MyCellAPI
//
//  Created by Samet Yatmaz on 21.05.2020.
//  Copyright © 2020 Samet Yatmaz. All rights reserved.
//

import Foundation

public struct PhoneCell: Decodable, Equatable {

    public enum CodingKeys: String, CodingKey {
        case name
        case price
        case planAttributes
    }

    public let name: String
    public let price: Int
    public let planAttributes: [Attribute]

}

public struct Attribute: Decodable, Equatable {
    public let code: String
    public let value: String
    public let subtext: String
}

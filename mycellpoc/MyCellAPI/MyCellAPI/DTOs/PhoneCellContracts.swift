//
//  PhoneCellContracts.swift
//  MyCellAPI
//
//  Created by Samet Yatmaz on 21.05.2020.
//  Copyright © 2020 Samet Yatmaz. All rights reserved.
//

import Foundation

public protocol PhoneCellServiceProtocol: class {
    var delegate: PhoneCellServiceDelegate? { get  set }
    func fetchData()
}

public protocol PhoneCellServiceDelegate: class {
    func getResult(_ result: Result<PhoneCellResponse>)
}

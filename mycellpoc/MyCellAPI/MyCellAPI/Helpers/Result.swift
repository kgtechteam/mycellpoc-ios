//
//  Result.swift
//  MyCellAPI
//
//  Created by Samet Yatmaz on 21.05.2020.
//  Copyright © 2020 Samet Yatmaz. All rights reserved.
//

import Foundation

public enum Result<Value> {
    case success(Value)
    case failure(Error)
}

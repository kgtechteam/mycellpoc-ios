//
//  PhoneCellResponse.swift
//  MyCellAPI
//
//  Created by Samet Yatmaz on 21.05.2020.
//  Copyright © 2020 Samet Yatmaz. All rights reserved.
//

import Foundation

public struct PhoneCellResponse: Decodable {
    
    public let results: [PhoneCell]
    
    public init(results: [PhoneCell]) {
        self.results = results
    }
    
    private enum AttributeCodingKey: String, CodingKey {
        case planAttributes
    }
    
    public init(from decoder: Decoder) throws {
        let rootContainer = try decoder.singleValueContainer()
        self.results = try rootContainer.decode([PhoneCell].self)
    }
}

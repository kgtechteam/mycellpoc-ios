//
//  PhoneCellService.swift
//  MyCellAPI
//
//  Created by Samet Yatmaz on 21.05.2020.
//  Copyright © 2020 Samet Yatmaz. All rights reserved.
//

import Foundation

public final class PhoneCellService: PhoneCellServiceProtocol {
    public weak var delegate: PhoneCellServiceDelegate?
    
    public init() {}
    
    public enum Error: Swift.Error {
        case serializationError(internal: Swift.Error)
        case networkError(internal: Swift.Error)
    }
    
    public func fetchData() {
        let stringUrl = "http://188.166.70.175:8080/api/plan/all"
        if let url = URL(string: stringUrl) {
            let session = URLSession(configuration: .default)
            let dataTask = session.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    self.delegate?.getResult(.failure(Error.networkError(internal: error)))
                }
                if let data = data {
                    let decoder = JSONDecoder()
                    do {
                        let response = try decoder.decode(PhoneCellResponse.self, from: data)
                        self.delegate?.getResult(.success(response))
                    } catch {
                        self.delegate?.getResult(.failure(Error.serializationError(internal: error)))
                    }
                }
            }
            dataTask.resume()
        }
    }
}

//
//  MyCellAPITests.swift
//  MyCellAPITests
//
//  Created by Samet Yatmaz on 21.05.2020.
//  Copyright © 2020 Samet Yatmaz. All rights reserved.
//

import XCTest
import MyCellAPI

class MyCellAPITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testRunTest() {
        MyCellAPIClient.testRun()
    }
    
    func testAPI()throws {
        let bundle = Bundle(for: MyCellAPITests.self)
        guard let url = bundle.url(forResource: "phoneCell", withExtension: "json") else { return }
        let data = try Data(contentsOf: url)
        let decoder = JSONDecoder()
        let phoneCell: [PhoneCell] = try decoder.decode([PhoneCell].self, from: data)
        
        let phoneCellFirst = phoneCell[0]
        let phoneCellSecond = phoneCell[1]
        
        XCTAssertEqual(phoneCellFirst.name, "MyCell Small")
        XCTAssertEqual(phoneCellFirst.planAttributes.first!.code, "internet")
        XCTAssertEqual(phoneCellFirst.planAttributes.first!.value, "3 GB")
        XCTAssertEqual(phoneCellFirst.planAttributes.first!.subtext, "TO USE ON ANYTHING")
        
        XCTAssertEqual(phoneCellSecond.name, "MyCell Large")
        XCTAssertEqual(phoneCellSecond.planAttributes[1].code, "minutes")
        XCTAssertEqual(phoneCellSecond.planAttributes[1].value, "1000 DK")
        XCTAssertEqual(phoneCellSecond.planAttributes[1].subtext, "ON DIGITAL SERVICES")
        
    }

}

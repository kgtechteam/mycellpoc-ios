//
//  MainViewController.swift
//  mycellpoc
//
//  Created by Emirhan on 12.12.2019.
//  Copyright © 2019 KGTeknoloji. All rights reserved.
//

import UIKit
import MyCellAPI

class MainViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var actionsTableview: UITableView!
    
    
    var cellTitles = ["HAT SİPARİŞLERİM", "SIK SORULAN SORULAR", "MYCELL ASİSTAN"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //API test
        MyCellAPIClient.testRun()
        
        titleLabel.updateGradientTextColor(gradientColors: [UIColor.red, UIColor.blue])
        

        configureUI()
        // Do any additional setup after loading the view.
    }
    
    func configureUI(){
        
        profileImage.layer.cornerRadius = profileImage.frame.width / 2
        
        self.actionsTableview.layer.shadowColor = UIColor.lightGray.cgColor
        self.actionsTableview.layer.shadowOpacity = 0.5
        self.actionsTableview.layer.shadowRadius = 10.0
        self.actionsTableview.layer.shadowOffset = .zero
        
        self.actionsTableview.layer.shouldRasterize = true
        
        self.actionsTableview.clipsToBounds = false
        self.actionsTableview.layer.masksToBounds = false
        self.actionsTableview.layer.cornerRadius = 13.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "one") as? MainScreenTableViewCell
        cell?.cellTitle.text = cellTitles[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
}


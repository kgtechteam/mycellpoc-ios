//
//  PhoneCellListContracts.swift
//  mycellpoc
//
//  Created by Samet Yatmaz on 22.05.2020.
//  Copyright © 2020 KGTeknoloji. All rights reserved.
//

import Foundation

public protocol PhoneCellListViewModelProtocol: class {
    var delegate: PhoneCellListViewModelDelegate? {get set}
    func load()
}

public protocol PhoneCellListViewModelDelegate: class {
    func showList(presentations: [PhoneCellPresentation])
}

//
//  PhoneCellListViewController.swift
//  mycellpoc
//
//  Created by Samet Yatmaz on 22.05.2020.
//  Copyright © 2020 KGTeknoloji. All rights reserved.
//

import UIKit
import MyCellAPI

class PhoneCellListViewController: UIViewController {

    @IBOutlet weak var firstView: UIView!
    @IBOutlet var buttonLabels: [UILabel]!
    @IBOutlet weak var packageTable: UITableView!
    
    private var viewModel: PhoneCellListViewModelProtocol = PhoneCellListViewModel()
    
    private var phoneCellPresentation: [PhoneCellPresentation] = []
    private var tableData = [[Any]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI configure
        configureUI()
        
        // View Model preperation
        viewModel.delegate = self
        viewModel.load()
        
    }
    

}

extension PhoneCellListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        var resultCell: UITableViewCell?
        
        if indexPath.section == 0{
            let cell = Bundle.main.loadNibNamed("TopCellTableViewCell", owner: self, options: nil)?.first as? TopCellTableViewCell
            resultCell = cell
            
        }
        else{
            let cell = Bundle.main.loadNibNamed("CardTableViewCell", owner: self, options: nil)?.first as? CardTableViewCell
     
            let obj = tableData[indexPath.section][indexPath.row] as! PhoneCellPresentation
    
            
            cell?.cardTitle.text = obj.name
            cell?.appLabel.text = obj.attributes.last!.value
            cell?.dataLabel.text = obj.attributes.first!.value
            cell?.minuteInfo.text = obj.attributes[1].value
            cell?.priceBig.text = String(obj.price)

            cell!.colorView.layoutIfNeeded()
            
            let hexStringEnd: String
            let hexStringStart: String
            
            switch indexPath.row {
            case 0,3:
                hexStringEnd = "#fad047"
                hexStringStart = "#f6974f"
            case 1,4:
                hexStringEnd = "#f28241"
                hexStringStart = "#ef4962"
            case 2,5:
                hexStringEnd = "#e53763"
                hexStringStart = "#af55f9"
            default:
                hexStringEnd = "#f28241"
                hexStringStart = "#ef4962"
            }
            
            cell?.colorView.endColor = UIColor(hexString: hexStringEnd)!
            cell?.colorView.startColor = UIColor(hexString: hexStringStart)!
            cell?.colorView.rectCornerList = [.bottomRight, .bottomLeft]
            
            cell?.addGradient(firstColor: UIColor(hexString: hexStringEnd)!, secondColor: UIColor(hexString: hexStringStart)!)
            
            resultCell = cell
        }

        return resultCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableData[indexPath.section].count == 1{
            return 140
        }
        else{
            return 332
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count
    }
}

extension PhoneCellListViewController: PhoneCellListViewModelDelegate {
    func showList(presentations: [PhoneCellPresentation]) {
        self.phoneCellPresentation = presentations
        DispatchQueue.main.async {
            self.tableData.append([""])
            self.tableData.append(self.phoneCellPresentation)
            print(self.tableData)
            self.packageTable.contentInset.bottom = self.tabBarController?.tabBar.frame.height ?? 0
            self.packageTable.reloadData()
        }
    }
}

extension PhoneCellListViewController {
    func configureUI(){
        for label in buttonLabels{
            label.layer.masksToBounds = true
            label.layer.cornerRadius = label.frame.width / 2
        }
        
        firstView.layer.cornerRadius = 20.0
        
        
        let topView = UIView()
        topView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: 50)
        topView.backgroundColor = UIColorFromRGB(rgbValue: 0xda855a)
        self.view.addSubview(topView)
        topView.layer.zPosition = -1
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}


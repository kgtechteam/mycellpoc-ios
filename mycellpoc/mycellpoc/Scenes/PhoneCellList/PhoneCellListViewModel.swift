//
//  PhoneCellListViewModel.swift
//  mycellpoc
//
//  Created by Samet Yatmaz on 22.05.2020.
//  Copyright © 2020 KGTeknoloji. All rights reserved.
//

import Foundation
import MyCellAPI

public final class PhoneCellListViewModel: PhoneCellListViewModelProtocol {
    public weak var delegate: PhoneCellListViewModelDelegate?
    private var service: PhoneCellServiceProtocol
    private var phoneCells: [PhoneCell] = []
    
    public init() {
        service = PhoneCellService()
        service.delegate = self
    }
    
    public func load() {
        service.fetchData()
    }
}


extension PhoneCellListViewModel: PhoneCellServiceDelegate {
    public func getResult(_ result: Result<PhoneCellResponse>) {
        switch result {
        case .success(let response):
            print(response.results)
            let values = response.results
            let phoneCellPresentation: [PhoneCellPresentation] = values.map{PhoneCellPresentation.init(phoneCell: $0)}
            delegate?.showList(presentations: phoneCellPresentation)
        case .failure(let error):
            print(error)
        }
    }
}

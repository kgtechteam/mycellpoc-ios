//
//  PhoneCellPresentation.swift
//  mycellpoc
//
//  Created by Samet Yatmaz on 22.05.2020.
//  Copyright © 2020 KGTeknoloji. All rights reserved.
//

import Foundation
import MyCellAPI

public final class PhoneCellPresentation: NSObject {
    
    public let name: String
    public let price: Int
    public let attributes: [AttributePresentation]
   
    init(name: String, price: Int, attributes: [Attribute]) {
        self.name = name
        self.price = price
        self.attributes = attributes.map{AttributePresentation(attribute: $0)}
        super.init()
    }
}

extension PhoneCellPresentation {
    convenience init(phoneCell: PhoneCell) {
        self.init(name: phoneCell.name, price: phoneCell.price, attributes: phoneCell.planAttributes)
    }
}


public final class AttributePresentation: NSObject {
    public let code: String
    public let value: String
    public let subtext: String
    
    init(code: String, value: String, subtext: String) {
        self.code = code
        self.value = value
        self.subtext = subtext
        super.init()
    }
}

extension AttributePresentation {
    convenience init(attribute: Attribute) {
        self.init(code: attribute.code, value: attribute.value, subtext: attribute.subtext)
    }
}


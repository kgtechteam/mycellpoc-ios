//
//  MyCellTabBarController.swift
//  mycellpoc
//
//  Created by Emirhan on 12.12.2019.
//  Copyright © 2019 KGTeknoloji. All rights reserved.
//

import UIKit
import MyCellAPI

class MyCellTabBarController: UITabBarController, TabBarDelegate {
    
    
    
    let mainTabBar = TabBarView()
    private var bottomConstraint: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        addTabBar()
        // Do any additional setup after loading the view.
    }
    
    
    private func addTabBar() {
        mainTabBar.delegate = self
        self.view.addSubview(mainTabBar)
        
        mainTabBar.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraint = mainTabBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        let trailingConstraint = mainTabBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        bottomConstraint = mainTabBar.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor)
        let heightConstraint = mainTabBar.heightAnchor.constraint(equalToConstant: 90)
        self.view.addConstraints([leadingConstraint, trailingConstraint, bottomConstraint!, heightConstraint])
        
        //addSpaceView()
    }
    
    
    func tabTapped(tabIndex: Int) {
        print(tabIndex)
        self.selectedIndex = tabIndex
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

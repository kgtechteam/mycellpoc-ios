//
//  CardTableViewCell.swift
//  mycellpoc
//
//  Created by Emirhan on 13.12.2019.
//  Copyright © 2019 KGTeknoloji. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardTitle: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var callLalbel: UILabel!
    @IBOutlet weak var smsInfo: UILabel!
    @IBOutlet weak var minuteInfo: UILabel!
    @IBOutlet weak var priceBig: UILabel!
    @IBOutlet weak var priceSmall: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var topColorView: UIView!
    @IBOutlet weak var appLabel: UILabel!
    @IBOutlet weak var colorView: GradientView!
    
    @IBOutlet weak var cardView: UIView!
    
    var gc = CAGradientLayer()
    var topViewColors = [UIColor]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cardView.layer.cornerRadius = 13.0
        self.cardView.layer.shadowColor = UIColor.lightGray.cgColor
        self.cardView.layer.shadowOpacity = 0.5
        self.cardView.layer.shadowRadius = 10.0
        self.cardView.layer.shadowOffset = .zero
        
        self.cardView.layer.shouldRasterize = true

        topColorView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5.0)
        colorView.layer.zPosition = -1
        selectButton.layer.cornerRadius = 15
    }
    
    func addGradient(firstColor: UIColor, secondColor: UIColor){
        let gradient = CAGradientLayer()
        gradient.frame = topColorView.bounds
        gradient.colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        topColorView.layer.insertSublayer(gradient, at: 0)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

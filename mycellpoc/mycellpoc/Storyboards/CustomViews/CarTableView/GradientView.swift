//
//  GradientView.swift
//  mycellpoc
//
//  Created by Emirhan Kutlu on 14.12.2019.
//  Copyright © 2019 KGTeknoloji. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
    @IBInspectable var endColor: UIColor = .blue { didSet { updateColors() }}
    @IBInspectable var startColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    var rectCornerList = UIRectCorner()
    
    override func awakeFromNib() {
        endColor = UIColor.blue
    }
    
    func updatePoints() {
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
    }
    func updateLocations() {
        //gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners(corners: rectCornerList, radius: 10.0)
        updatePoints()
        updateLocations()
        updateColors()
    }
    
}

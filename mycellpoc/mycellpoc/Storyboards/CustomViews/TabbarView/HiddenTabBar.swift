//
//  HiddenTabBar.swift
//  mycellpoc
//
//  Created by Emirhan on 12.12.2019.
//  Copyright © 2019 KGTeknoloji. All rights reserved.
//

import UIKit

class HiddenTabBar: UITabBar {

    override var isHidden: Bool {
        set {
            super.isHidden = true
        }
        get {
            return super.isHidden
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

//
//  TabBarView.swift
//  mycellpoc
//
//  Created by Emirhan on 12.12.2019.
//  Copyright © 2019 KGTeknoloji. All rights reserved.
//

import UIKit

class TabBarView: UIView {

    weak var delegate: TabBarDelegate?
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet var tabButtons: [UIButton]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        loadViewFromNib(nibName: "TabBarView")
        
        mainButton.layer.cornerRadius = mainButton.frame.width / 2
    }
    
    @IBAction func tabTapped(_ sender: UIButton) {
        delegate?.tabTapped(tabIndex: sender.tag)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
class CustomButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    


}
